package org.bof.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.NoSuchElementException;
import java.util.Optional;

import org.junit.jupiter.api.Test;

class WrapperCoreTest {

    private static final String TEST_DATA_STRING = "foobar";

    @Test
    void testOfWithNonNullValue() {
        Wrapper<String> wrapper = Wrapper.of(TEST_DATA_STRING);
        assertTrue(wrapper.isPresent());
    }

    @Test
    void testOfWithNullValue() {
        assertThrows(NullPointerException.class, () -> Wrapper.of(null));
    }

    @Test
    void testOfNullableWithNonNullValue() {
        Wrapper<String> wrapper = Wrapper.ofNullable(TEST_DATA_STRING);
        assertTrue(wrapper.isPresent());
    }

    @Test
    void testOfNullableWithNullValue() {
        Wrapper<Object> wrapper = Wrapper.ofNullable(null);
        assertFalse(wrapper.isPresent());
    }

    @Test
    void testFromOptionalWithNonNullValue() {
        Wrapper<String> wrapper = Wrapper.fromOptional(Optional.of(TEST_DATA_STRING));
        assertTrue(wrapper.isPresent());
    }

    @Test
    void testFromOptionalWithEmptyOptional() {
        Wrapper<Object> wrapper = Wrapper.fromOptional(Optional.empty());
        assertFalse(wrapper.isPresent());
    }

    @Test
    void testIsEmptyOnEmptyInstance() {
        assertTrue(Wrapper.empty().isEmpty());
    }

    @Test
    void testIsPresentOnNonEmptyInstance() {
        Wrapper<String> wrapper = Wrapper.of(TEST_DATA_STRING);
        assertTrue(wrapper.isPresent());
    }

    @Test
    void testGetOnNonEmptyInstance() {
        Wrapper<String> wrapper = Wrapper.of(TEST_DATA_STRING);
        assertEquals(TEST_DATA_STRING, wrapper.get());
    }

    @Test
    void testGetOnEmptyInstance() {
        Wrapper<?> target = Wrapper.empty();
        assertThrows(NoSuchElementException.class, () -> target.get());
    }

    @Test
    void testIfPresentOnNonEmptyInstance() {
        Wrapper<String> wrapper = Wrapper.of(TEST_DATA_STRING);
        wrapper.ifPresent(v -> assertEquals(TEST_DATA_STRING, v));
    }

    @Test
    void testIfPresentOrElseWithPresentValue() {
        String value = "test";
        Wrapper<String> wrapper = Wrapper.of(value);
        wrapper.ifPresentOrElse(
                v -> assertEquals(value, v),
                () -> fail("Should not execute empty action"));
    }

    @Test
    void testIfPresentOrElseWithAbsentValue() {
        Wrapper<String> wrapper = Wrapper.empty();
        wrapper.ifPresentOrElse(
                v -> fail("Should not execute value action"),
                () -> assertTrue(true) // Succeed if empty action is executed
        );
    }

    @Test
    void testMapOnNonEmptyInstance() {
        Wrapper<String> wrapper = Wrapper.of(TEST_DATA_STRING);
        Wrapper<Integer> mapped = wrapper.map(String::length);
        assertTrue(mapped.isPresent());
        assertEquals(TEST_DATA_STRING.length(), mapped.get());
    }

    @Test
    void testFlatMapOnNonEmptyInstance() {
        Wrapper<String> wrapper = Wrapper.of(TEST_DATA_STRING);
        Wrapper<String> flatMapped = wrapper.flatMap(v -> Wrapper.of(v.toUpperCase()));
        assertTrue(flatMapped.isPresent());
        assertEquals(TEST_DATA_STRING.toUpperCase(), flatMapped.get());
    }

    @Test
    void testFilterOnMatchingPredicate() {
        Wrapper<Integer> wrapper = Wrapper.of(10);
        Wrapper<Integer> filtered = wrapper.filter(v -> v > 5);
        assertTrue(filtered.isPresent());
    }

    @Test
    void testFilterOnNonMatchingPredicate() {
        Wrapper<Integer> wrapper = Wrapper.of(4);
        Wrapper<Integer> filtered = wrapper.filter(v -> v > 5);
        assertFalse(filtered.isPresent());
    }

    @Test
    void testOrElseWithAbsentValue() {
        String result = Wrapper.<String>empty().orElse(TEST_DATA_STRING);
        assertEquals(TEST_DATA_STRING, result);
    }

    @Test
    void testOrElseGetWithAbsentValue() {
        String result = Wrapper.<String>empty().orElseGet(() -> TEST_DATA_STRING);
        assertEquals(TEST_DATA_STRING, result);
    }

    @Test
    void testOrElseThrowWithAbsentValue() {
        Wrapper<?> target = Wrapper.empty();
        assertThrows(NoSuchElementException.class, () -> target.orElseThrow());
    }

    @Test
    void testStreamWithAbsentValue() {
        assertEquals(0L, Wrapper.empty().stream().count());
    }

    @Test
    void testEquals() {
        Wrapper<String> wrapper1 = Wrapper.of(TEST_DATA_STRING);
        Wrapper<String> wrapper2 = Wrapper.of(TEST_DATA_STRING);
        assertEquals(wrapper1, wrapper2);
    }

    @Test
    void testHashCodeOnPresentValue() {
        Wrapper<String> wrapper = Wrapper.of(TEST_DATA_STRING);
        assertEquals(TEST_DATA_STRING.hashCode(), wrapper.hashCode());
    }

    @Test
    void testHashCodeOnEmpty() {
        assertEquals(0, Wrapper.empty().hashCode());
    }

}
