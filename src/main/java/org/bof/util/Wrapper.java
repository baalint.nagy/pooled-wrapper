package org.bof.util;

import static java.util.Objects.requireNonNull;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * A container object which may or may not contain a non-{@code null} value. If
 * a value is present, {@code isPresent()} returns {@code true}. If no value is
 * present, the object is considered <i>empty</i> and {@code isPresent()}
 * returns {@code false}.
 *
 * <p>
 * Additional methods that depend on the presence or absence of a contained
 * value are provided, such as {@link #orElse(Object) orElse()} (returns a
 * default value if no value is present) and {@link #ifPresent(Consumer)
 * ifPresent()} (performs an action if a value is present).
 *
 * <p>
 * This is a <a href="
 * {@docRoot}/java.base/java/lang/doc-files/ValueBased.html">value-based</a> class;
 * programmers should treat instances that are {@linkplain #equals(Object)
 * equal} as interchangeable and should not use instances for synchronization,
 * or unpredictable behavior may occur. For example, in a future release,
 * synchronization may fail.
 *
 * @apiNote {@code Wrapper} is primarily intended for use as a utility inside
 *          method implementation to handle {@node null}-able values A variable
 *          whose type is {@code Wrapper} should never itself be {@code null};
 *          it should always point to an {@code Wrapper} instance.
 *
 * @param <T> the type of value
 */
public class Wrapper<T> {

    private static final String POOL_SIZE_ENV = "WRAPPER_POOL_SIZE";
    private static final String WRAPPER_CONFIG = "wrapper-config.conf";
    private static final String POOL_SIZE_KEY = "poolSize";

    private static int poolSize = 32;
    private static ThreadLocal<Wrapper<?>[]> pool;

    static {
        try {
            // Try environment variable
            String poolSizeEnv = System.getenv(POOL_SIZE_ENV);
            if (poolSizeEnv != null && !poolSizeEnv.isEmpty()) {
                poolSize = Integer.parseInt(poolSizeEnv);
            } else {
                // Try VM argument
                String poolSizeVmArg = System.getProperty(POOL_SIZE_KEY);
                if (poolSizeVmArg != null && !poolSizeVmArg.isEmpty()) {
                    poolSize = Integer.parseInt(poolSizeVmArg);
                } else {
                    // Properties file
                    Properties properties = new Properties();
                    properties.load(Files.newInputStream(Paths.get(WRAPPER_CONFIG)));
                    String poolSizeProp = properties.getProperty(POOL_SIZE_KEY);
                    if (poolSizeProp != null && !poolSizeProp.isEmpty()) {
                        poolSize = Integer.parseInt(poolSizeProp);
                    }
                }
            }
        } catch (Exception e) {
            poolSize = 32;
        }

        // Initialize Pool
        pool = ThreadLocal.withInitial(() -> {
            Wrapper<?>[] poolArray = new Wrapper<?>[poolSize];
            for (int i = 0; i < poolSize; ++i) {
                poolArray[i] = new Wrapper<>(true, null);
            }
            return poolArray;
        });
    }

    /**
     * Returns a {@code Wrapper} describing the given non-{@code null} value.
     *
     * @param value the value to describe, which must be non-{@code null}
     * @param <T>   the type of the value
     * @return a {@code Wrapper} with the value present
     * @throws NullPointerException if value is {@code null}
     */
    public static <T> Wrapper<T> of(T value) {
        return create(requireNonNull(value));
    }

    /**
     * Returns a {@code Wrapper} describing the given value, if non-{@code null},
     * otherwise returns an empty {@code Wrapper}.
     *
     * @param value the possibly-{@code null} value to describe
     * @param <T> the type of the value
     * @return an {@code Wrapper} with a present value if the specified value is
     *         non-{@code null}, otherwise an empty {@code Wrapper}
     */
    public static <T> Wrapper<T> ofNullable(T value) {
        return create(value);
    }

    public static <T> Wrapper<T> fromOptional(Optional<T> optional) {
        return new Wrapper<>(false, requireNonNull(optional, "optional").orElse(null));
    }

    protected static <T> Wrapper<T> create(T input) {
        Wrapper<?>[] poolArray = pool.get();
        for (int i = 0; i < poolSize; ++i) {
            Wrapper<?> it = poolArray[i];
            if (it.free) {
                it.free = false;
                it.value = input;
                return cast(it);
            }
        }
        throw new IllegalStateException("Wrapper pool depleted!");
    }

    private static final Wrapper<?> EMPTY = new Wrapper<>();

    public static <T> Wrapper<T> empty() {
        return cast(EMPTY);
    }

    protected boolean free = true;
    protected Object value = null;

    private Wrapper() {
    }

    private Wrapper(boolean free, Object value) {
        this.free = free;
        this.value = value;
    }

    public Optional<T> toOptional() {
        return Optional.ofNullable(cast(value));
    }

    /**
     * If a value is present, returns the value, otherwise throws
     * {@code NoSuchElementException}.
     *
     * Releases this instance!
     *
     * @apiNote The preferred alternative to this method is {@link #orElseThrow()}.
     *
     * @return the non-{@code null} value described by this {@code Wrapper}
     * @throws NoSuchElementException if no value is present
     */
    public T get() {
        return orElseThrow();
    }

    /**
     * If a value is present, returns {@code true}, otherwise {@code false}.
     *
     * @return {@code true} if a value is present, otherwise {@code false}
     */
    public boolean isPresent() {
        return value != null;
    }

    /**
     * If a value is not present, returns {@code true}, otherwise {@code false}.
     *
     * @return {@code true} if a value is not present, otherwise {@code false}
     */
    public boolean isEmpty() {
        return value == null;
    }

    /**
     * If a value is present, performs the given action with the value, otherwise
     * does nothing. Releases this instance!
     *
     * @param action the action to be performed, if a value is present
     * @throws NullPointerException if value is present and the given action is
     * {@code null}
     */
    public void ifPresent(Consumer<? super T> action) {
        requireNonNull(action, "action");
        free = true;
        if (value != null) {
            action.accept(cast(value));
        }
    }

    /**
     * If a value is present, performs the given action with the value, otherwise
     * performs the given empty-based action.
     *
     * Releases this instance!
     *
     * @param action      the action to be performed, if a value is present
     * @param emptyAction the empty-based action to be performed, if no value is
     *                    present
     * @throws NullPointerException the given action is {@code null}, the given
     *                              emptyAction is {@code null}.
     */
    public void ifPresentOrElse(Consumer<? super T> action, Runnable emptyAction) {
        requireNonNull(action, "action");
        requireNonNull(emptyAction, "emptyAction");
        free = true;
        if (value != null) {
            action.accept(cast(value));
        } else {
            emptyAction.run();
        }
    }

    /**
     * If a value is present, and the value matches the given predicate, returns an
     * {@code Wrapper} describing the value, otherwise returns an empty
     * {@code Wrapper}.
     *
     * @param predicate the predicate to apply to a value, if present
     * @return an {@code Wrapper} describing the value of this {@code Wrapper}, if a
     *         value is present and the value matches the given predicate, otherwise
     *         an empty {@code Wrapper}
     * @throws NullPointerException if the predicate is {@code null}
     */
    public Wrapper<T> filter(Predicate<? super T> predicate) {
        if (!requireNonNull(predicate, "predicate").test(cast(value))) {
            value = null;
        }
        return this;
    }

    /**
     * If a value is present, returns an {@code Wrapper} describing (as if by
     * {@link #ofNullable}) the result of applying the given mapping function to the
     * value, otherwise returns an empty {@code Wrapper}.
     *
     * <p>
     * If the mapping function returns a {@code null} result then this method
     * returns an empty {@code Wrapper}.
     *
     * @apiNote This method supports post-processing on {@code Wrapper} values,
     *          without the need to explicitly check for a return status. For
     *          example, the following code traverses a stream of URIs, selects one
     *          that has not yet been processed, and creates a path from that URI,
     *          returning an {@code Wrapper<Path>}:
     *
     *          <pre>
     *     Wrapper<Path> p = uris.stream().filter(uri -> !isProcessedYet(uri))
     *         .findFirst()
     *         .map(Paths::get);
     * }
     *          </pre>
     *
     *          Here, {@code findFirst} returns an {@code Wrapper<URI>}, and then
     *          {@code map} returns an {@code Wrapper<Path>} for the desired URI if
     *          one exists.
     *
     * @param mapper the mapping function to apply to a value, if present
     * @param <R>    The type of the value returned from the mapping function
     * @return an {@code Wrapper} describing the result of applying a mapping
     *         function to the value of this {@code Wrapper}, if a value is present,
     *         otherwise an empty {@code Wrapper}
     * @throws NullPointerException if the mapping function is {@code null}
     */
    public <R> Wrapper<R> map(Function<? super T, ? extends R> mapper) {
        requireNonNull(mapper, "mapper");
        if (value != null) {
            value = mapper.apply(cast(value));
        }
        return cast(this);
    }

    /**
     * If a value is present, returns the result of applying the given
     * {@code Wrapper}-bearing mapping function to the value, otherwise returns an
     * empty {@code Wrapper}.
     *
     * Releases this instance!
     *
     * <p>
     * This method is similar to {@link #map(Function)}, but the mapping function is
     * one whose result is already a {@code Wrapper}, and if invoked,
     * {@code flatMap} does not wrap it within an additional {@code Wrapper}.
     *
     * @param <R>The type of value of the {@code Wrapper} returned by the mapping
     *               function
     * @param mapper the mapping function to apply to a value, if present
     * @return the result of applying an {@code Wrapper}-bearing mapping function to
     *         the value of this {@code Wrapper}, if a value is present, otherwise
     *         an empty {@code Wrapper}
     * @throws NullPointerException if the mapping function is {@code null} or
     *                              returns a {@code null} result
     */
    public <R> Wrapper<R> flatMap(Function<? super T, ? extends Wrapper<? extends R>> mapper) {
        requireNonNull(mapper, "mapper");
        free = true;
        if (value == null) {
            return cast(EMPTY);
        }
        return cast(requireNonNull(mapper.apply(cast(value)), "return value of flatMap"));
    }

    /**
     * If a value is present, returns a {@code Wrapper} describing the value,
     * otherwise returns a {@code Wrapper} produced by the supplying function.
     *
     * Releases this instance, if supplier was used!
     *
     * @param supplier the supplying function that produces a {@code Wrapper} to be
     *                 returned
     * @return returns an {@code Wrapper} describing the value of this
     *         {@code Wrapper}, if a value is present, otherwise a {@code Wrapper}
     *         produced by the supplying function.
     * @throws NullPointerException if the supplying function is {@code null} or
     *                              produces a {@code null} result
     */
    public Wrapper<T> or(Supplier<? extends Wrapper<? extends T>> supplier) {
        requireNonNull(supplier, "supplier");
        if (value == null) {
            free = true;
            return cast(requireNonNull(supplier.get(), "return value of or"));
        }
        return this;
    }

    /**
     * If a value is present, returns a sequential {@link Stream} containing only
     * that value, otherwise returns an empty {@code Stream}.
     *
     * Releases this instance!
     *
     * @apiNote This method can be used to transform a {@code Stream} of optional
     *          elements to a {@code Stream} of present value elements:
     *
     *          <pre>{@code
     *     Stream<Wrapper<T>> os = ..
     *     Stream<T> s = os.flatMap(Wrapper::stream)
     * }</pre>
     *
     * @return the optional value as a {@code Stream}
     * @since 9
     */
    public Stream<T> stream() {
        free = true;
        if (value == null) {
            return Stream.empty();
        }
        return Stream.of(cast(value));
    }

    /**
     * If a value is present, returns the value, otherwise returns {@code other}.
     *
     * Releases this instance!
     *
     * @param other the value to be returned, if no value is present. May be
     *              {@code null}.
     * @return the value, if present, otherwise {@code other}
     */
    public T orElse(T other) {
        free = true;
        if (value == null) {
            return other;
        }
        return cast(value);
    }

    /**
     * If a value is present, returns the value, otherwise returns the result
     * produced by the supplying function.
     *
     * Releases this instance!
     *
     * @param supplier the supplying function that produces a value to be returned
     * @return the value, if present, otherwise the result produced by the supplying
     *         function
     * @throws NullPointerException if no value is present and the supplying
     *                              function is {@code null}
     */
    public T orElseGet(Supplier<? extends T> supplier) {
        requireNonNull(supplier, "supplier");
        free = true;
        return value != null ? cast(value) : cast(supplier.get());
    }

    /**
     * If a value is present, returns the value, otherwise throws
     * {@code NoSuchElementException}.
     *
     * Releases this instance!
     *
     * @return the non-{@code null} value described by this {@code Wrapper}
     * @throws NoSuchElementException if no value is present
     */
    public T orElseThrow() {
        free = true;
        if (value == null) {
            throw new NoSuchElementException("No value present");
        }
        return cast(value);
    }

    /**
     * If a value is present, returns the value, otherwise throws an exception
     * produced by the exception supplying function.
     *
     * Releases this instance!
     *
     * @apiNote A method reference to the exception constructor with an empty
     *          argument list can be used as the supplier. For example,
     *          {@code IllegalStateException::new}
     *
     * @param <X>               Type of the exception to be thrown
     * @param exceptionSupplier the supplying function that produces an exception to
     *                          be thrown
     * @return the value, if present
     * @throws X                    if no value is present
     * @throws NullPointerException if no value is present and the exception
     *                              supplying function is {@code null}
     */
    public <X extends Throwable> T orElseThrow(Supplier<? extends X> exceptionSupplier) throws X {
        requireNonNull(exceptionSupplier, "exceptionSupplier");
        free = true;
        if (value != null) {
            return cast(value);
        }
        throw exceptionSupplier.get();
    }

    /**
     * Indicates whether some other object is "equal to" this {@code Wrapper}. The
     * other object is considered equal if:
     * <ul>
     * <li>it is also an {@code Wrapper} and;
     * <li>both instances have no value present or;
     * <li>the present values are "equal to" each other via {@code equals()}.
     * </ul>
     *
     * @param obj an object to be tested for equality
     * @return {@code true} if the other object is "equal to" this object otherwise
     *         {@code false}
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        return obj instanceof Wrapper<?>
                && Objects.equals(value, ((Wrapper<?>) obj).value);
    }

    /**
     * Returns the hash code of the value, if present, otherwise {@code 0} (zero) if
     * no value is present.
     *
     * @return hash code value of the present value or {@code 0} if no value is
     *         present
     */
    @Override
    public int hashCode() {
        return Objects.hashCode(value);
    }

    /**
     * Returns a non-empty string representation of this {@code Wrapper} suitable
     * for debugging. The exact presentation format is unspecified and may vary
     * between implementations and versions.
     *
     * @implSpec If a value is present the result must include its string
     *           representation in the result. Empty and present {@code Wrapper}s
     *           must be unambiguously differentiable.
     *
     * @return the string representation of this instance
     */
    @Override
    public String toString() {
        return value != null
                ? "Wrapper[" + value + "]"
                : "Wrapper.empty";
    }

    @SuppressWarnings("unchecked")
    private static <S> S cast(Object value) {
        return (S) value;
    }

}
