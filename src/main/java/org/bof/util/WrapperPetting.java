package org.bof.util;

import java.util.List;
import java.util.stream.IntStream;

public class WrapperPetting {

    public static void main(String[] args) {
        List<String> r = IntStream.range(0, 10000).parallel()
                .mapToObj(i -> (i % 2 == 0 ? null : "xxxx" + i))
                .map(s -> Wrapper.ofNullable(s).map(String::toUpperCase).orElse("NA"))
                .toList();
        System.out.println(r);
    }

}
